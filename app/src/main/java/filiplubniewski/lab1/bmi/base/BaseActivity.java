package filiplubniewski.lab1.bmi.base;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import butterknife.ButterKnife;

/**
 * Created by macbookpro on 23.03.2017.
 */

public class BaseActivity extends AppCompatActivity

{
    @Override
    public void setContentView(int layoutResID)
    {
        super.setContentView(layoutResID);
        ButterKnife.bind(this, this);
    }

    protected void hideKeyboard(View view)
    {
        InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void showToast(String msg)
    {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    protected void showSnackbar(String msg, View view)
    {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();
    }
}
