package filiplubniewski.lab1.bmi.bmiContract;

/**
 * Created by macbookpro on 16.03.2017.
 */

public interface BmiCalculator
{
    boolean isMassValid(float mass);
    boolean isHeightValid(float height);
    float countBMI(float mass, float height) throws IllegalArgumentException;
    int unitsCode();
}
