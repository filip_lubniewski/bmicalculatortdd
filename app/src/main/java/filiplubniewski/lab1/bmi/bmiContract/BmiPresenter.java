package filiplubniewski.lab1.bmi.bmiContract;

/**
 * Created by macbookpro on 28.03.2017.
 */

public interface BmiPresenter
{
    void passUserInputToBmiCalculator(String mass, String height);

    void changeCalculator(BmiCalculator calculator);

    boolean isSavingUserDataPossible();

    void saveUserData();
}
