package filiplubniewski.lab1.bmi.bmiContract;

import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;

/**
 * Created by macbookpro on 27.03.2017.
 */

public interface BmiView
{
    void showBmi(String bmiValue);

    void showMessage(@StringRes int messageId);

    void showMessage(String msg);

    void showInfoAboutBmi(@StringRes int infoId);

    void changeColorOfBmiResult(@ColorRes int colorId);

    void saveUserMassAndHeight(String mass, String height, int units);
}
