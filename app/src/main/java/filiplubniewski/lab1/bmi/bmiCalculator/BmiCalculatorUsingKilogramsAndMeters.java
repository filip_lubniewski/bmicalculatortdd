package filiplubniewski.lab1.bmi.bmiCalculator;

import filiplubniewski.lab1.bmi.BmiConstants;
import filiplubniewski.lab1.bmi.bmiContract.BmiCalculator;

/**
 * Created by macbookpro on 16.03.2017.
 */

public class BmiCalculatorUsingKilogramsAndMeters implements BmiCalculator
{
    public static final float MIN_MASS = 10f;
    public static final float MAX_MASS = 250f;
    public static final float MIN_HEIGHT = 0.5f;
    public static final float MAX_HEIGHT = 10f;

    @Override
    public boolean isMassValid(float mass)
    {
        return mass >= MIN_MASS && mass <= MAX_MASS;
    }

    @Override
    public boolean isHeightValid(float height)
    {
        return height >= MIN_HEIGHT && height <= MAX_HEIGHT;
    }

    @Override
    public float countBMI(float mass, float height) throws IllegalArgumentException
    {
        if (isHeightValid(height) && isMassValid(mass))
        {
            return mass / (height * height);
        }
        else
        {
            throw new IllegalArgumentException("Wrong mass or height. Should be (10 - 250kg) and (0.5 - 10m)");
        }
    }

    @Override
    public int unitsCode()
    {
        return BmiConstants.KG_AND_M;
    }
}
