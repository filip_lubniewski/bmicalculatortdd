package filiplubniewski.lab1.bmi.bmiCalculator;

import filiplubniewski.lab1.bmi.BmiConstants;
import filiplubniewski.lab1.bmi.bmiContract.BmiCalculator;

/**
 * Created by macbookpro on 29.03.2017.
 */

public class BmiCalculatorUsingPoundsAndFeet implements BmiCalculator
{
    public static final float MIN_MASS = 22f;
    public static final float MAX_MASS = 550f;
    public static final float MIN_HEIGHT = 1.64f;
    public static final float MAX_HEIGHT = 32.8f;

    @Override
    public boolean isMassValid(float mass)
    {
        return mass >= MIN_MASS && mass <= MAX_MASS;
    }

    @Override
    public boolean isHeightValid(float height)
    {
        return height >= MIN_HEIGHT && height <= MAX_HEIGHT;
    }

    @Override
    public float countBMI(float mass, float height) throws IllegalArgumentException
    {
        if (isHeightValid(height) && isMassValid(mass))
        {
            return mass * 4.88f / (height * height);
        }
        else
        {
            throw new IllegalArgumentException("Wrong mass or height. Should be (22lbs - 550lbs) and (1.64ft - 32.8ft)");
        }
    }

    @Override
    public int unitsCode()
    {
        return BmiConstants.LBS_AND_FT;
    }
}
