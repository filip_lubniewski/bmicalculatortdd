package filiplubniewski.lab1.bmi.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import butterknife.BindView;
import butterknife.OnClick;
import filiplubniewski.lab1.bmi.BmiConstants;
import filiplubniewski.lab1.bmi.R;
import filiplubniewski.lab1.bmi.base.BaseActivity;
import filiplubniewski.lab1.bmi.bmiCalculator.BmiCalculatorUsingKilogramsAndMeters;
import filiplubniewski.lab1.bmi.bmiCalculator.BmiCalculatorUsingPoundsAndFeet;
import filiplubniewski.lab1.bmi.bmiContract.BmiCalculator;
import filiplubniewski.lab1.bmi.bmiContract.BmiView;
import filiplubniewski.lab1.bmi.presenter.BmiActivityPresenter;

public class BmiActivity extends BaseActivity implements BmiView
{
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.edit_text_height)
    EditText mHeightEditText;
    @BindView(R.id.edit_text_mass)
    EditText mMassEditText;
    @BindView(R.id.button_count)
    Button mCountButton;
    @BindView(R.id.text_view_result)
    TextView mResultTextView;
    @BindView(R.id.text_view_units)
    TextView mUnitsTextView;
    @BindView(R.id.toggle_button_units)
    ToggleButton mUnitsToggleButton;
    @BindView(R.id.text_view_info_about_bmi)
    TextView mInfoAboutBmiTextView;

    public static final String BMI_RESULT = "BMI_RESULT";
    public static final String RESULT_TXT_COLOR = "RESULT_TXT_COLOR";
    public static final String HEIGHT = "HEIGHT";
    public static final String MASS = "MASS";
    public static final String BMI_INFO = "BMI_INFO";
    public static final String TOGGLE_STATE = "TOGGLE_STATE";
    public static final String SAVED_MASS = "SAVED_MASS";
    public static final String SAVED_HEIGHT = "SAVED_HEIGHT";
    public static final String SAVED_UNITS = "SAVED_UNITS";
    public static final String USER_SAVED_DATA = "USER_SAVED_DATA";

    private BmiActivityPresenter mPresenter;
    private BmiCalculator mBmiCalculatorKgM;
    private BmiCalculator mBmiCalculatorLbFt;

    private int mToggleUnitsState = BmiConstants.KG_AND_M;

    @ColorRes
    private int mResultTextColor = R.color.black;
    private SharedPreferences mSharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBmiCalculatorKgM = new BmiCalculatorUsingKilogramsAndMeters();
        mBmiCalculatorLbFt = new BmiCalculatorUsingPoundsAndFeet();

        mPresenter = new BmiActivityPresenter(this, mBmiCalculatorKgM);

        handleToggleButton();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        restoreData();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        restoreFieldsAfterScreenRotation(savedInstanceState);
    }

    private void handleToggleButton()
    {
        mUnitsToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {
                    setPoundsAndFeetMode();
                }
                else
                {
                    setKilogramsAndMetersMode();
                }
            }
        });
    }

    private void restoreData()
    {
        mSharedPref = getPreferences(Context.MODE_PRIVATE);

        if (mSharedPref.getBoolean(USER_SAVED_DATA, false))
        {
            mMassEditText.setText(mSharedPref.getString(SAVED_MASS, "0"));
            mHeightEditText.setText(mSharedPref.getString(SAVED_HEIGHT, "0"));
            setPreviousUnitsMode(mSharedPref.getInt(SAVED_UNITS, 0));
        }
    }

    private void restoreFieldsAfterScreenRotation(Bundle savedInstanceState)
    {
        if (savedInstanceState != null)
        {
            showBmi(savedInstanceState.getString(BMI_RESULT));
            mInfoAboutBmiTextView.setText(savedInstanceState.getString(BMI_INFO));
            changeColorOfBmiResult(savedInstanceState.getInt(getString(R.string.RESULT_TEXT_COLOR)));
            mMassEditText.setText(savedInstanceState.getString(MASS));
            mHeightEditText.setText(savedInstanceState.getString(HEIGHT));
            setPreviousUnitsMode(savedInstanceState.getInt(TOGGLE_STATE));
        }
    }

    @OnClick(R.id.button_count)
    public void calculateBmi(View view)
    {
        mPresenter.passUserInputToBmiCalculator(mMassEditText.getText().toString(),
                mHeightEditText.getText().toString());

        hideKeyboard(view);
        invalidateOptionsMenu();
    }

    @OnClick(R.id.activity_main)
    public void backgroundClick(View view)
    {
        hideKeyboard(view);
    }

    @Override
    public void showBmi(String bmiValue)
    {
        mResultTextView.setText(bmiValue);
    }

    @Override
    public void showMessage(@StringRes int messageId)
    {
        showSnackbar(getString(messageId), this.getCurrentFocus());
    }

    @Override
    public void showMessage(String msg)
    {
        showSnackbar(msg, this.getCurrentFocus());
    }

    @Override
    public void showInfoAboutBmi(@StringRes int infoId)
    {
        mInfoAboutBmiTextView.setText(getString(infoId));
    }

    @Override
    public void changeColorOfBmiResult(@ColorRes int colorId)
    {
        mResultTextColor = colorId;
        mResultTextView.setTextColor(ContextCompat.getColor(this, colorId));
    }

    @Override
    public void saveUserMassAndHeight(String mass, String height, int unitsUsedToCalculateBmi)
    {
        mSharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPref.edit();

        editor.putBoolean(USER_SAVED_DATA, true);
        editor.putString(SAVED_MASS, mass);
        editor.putString(SAVED_HEIGHT, height);
        editor.putInt(SAVED_UNITS, unitsUsedToCalculateBmi);

        editor.apply();

        showToast(getString(R.string.saved_info));
    }

    private void setPreviousUnitsMode(int state)
    {
        switch (state)
        {
            case BmiConstants.KG_AND_M:
                setKilogramsAndMetersMode();
                break;
            case BmiConstants.LBS_AND_FT:
                setPoundsAndFeetMode();
                break;
            default:
                setKilogramsAndMetersMode();
        }
    }

    private void setKilogramsAndMetersMode()
    {
        mToggleUnitsState = BmiConstants.KG_AND_M;
        mUnitsTextView.setText(getString(R.string.kilograms_and_meters));
        mPresenter.changeCalculator(mBmiCalculatorKgM);
    }

    private void setPoundsAndFeetMode()
    {
        mToggleUnitsState = BmiConstants.LBS_AND_FT;
        mUnitsToggleButton.setChecked(true);

        mUnitsTextView.setText(getString(R.string.pounds_and_feet));
        mPresenter.changeCalculator(mBmiCalculatorLbFt);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu, menu);

        if (!mPresenter.isSavingUserDataPossible())
        {
            menu.findItem(R.id.menu_save).setEnabled(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_save:
                mPresenter.saveUserData();
                return true;
            case R.id.menu_share:
                shareYourResult();
                return true;
            case R.id.menu_author:
                startActivity(new Intent(this, AboutAuthorActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void shareYourResult()
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my BMI result: " + mResultTextView.getText().toString());
        sendIntent.setType("text/plain");

        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_bmi)));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putString(BMI_RESULT, mResultTextView.getText().toString());
        outState.putInt(RESULT_TXT_COLOR, mResultTextColor);
        outState.putString(MASS, mMassEditText.getText().toString());
        outState.putString(HEIGHT, mHeightEditText.getText().toString());
        outState.putString(BMI_INFO, mInfoAboutBmiTextView.getText().toString());
        outState.putInt(TOGGLE_STATE, mToggleUnitsState);
    }
}
