package filiplubniewski.lab1.bmi.presenter;

import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;

import java.util.Locale;

import filiplubniewski.lab1.bmi.BmiConstants;
import filiplubniewski.lab1.bmi.R;
import filiplubniewski.lab1.bmi.bmiContract.BmiCalculator;
import filiplubniewski.lab1.bmi.bmiContract.BmiPresenter;
import filiplubniewski.lab1.bmi.bmiContract.BmiView;

/**
 * Created by macbookpro on 28.03.2017.
 */
public class BmiActivityPresenter implements BmiPresenter
{
    private BmiView view;
    private BmiCalculator bmiCalculator;

    private float bmiResult;
    private String currentMass;
    private String currentHeight;
    private boolean saveMassAndHeightOptionEnabled;

    public BmiActivityPresenter(BmiView view, BmiCalculator bmiCalculator)
    {
        this.view = view;
        this.bmiCalculator = bmiCalculator;
    }

    @Override
    public void passUserInputToBmiCalculator(String mass, String height)
    {
        if (mass.isEmpty() || height.isEmpty())
        {
            view.showMessage(R.string.message_empty_input);
        }
        else
        {
            calculateBmiAndPassItToTheView(mass, height);
        }
    }

    @Override
    public void changeCalculator(BmiCalculator calculator)
    {
        bmiCalculator = calculator;
    }

    @Override
    public boolean isSavingUserDataPossible()
    {
        return saveMassAndHeightOptionEnabled;
    }

    @Override
    public void saveUserData()
    {
        view.saveUserMassAndHeight(currentMass, currentHeight, bmiCalculator.unitsCode());
    }

    private void calculateBmiAndPassItToTheView(String mass, String height)
    {
        boolean successfullyCalculated;
        String optionalErrorMessage = "";

        try
        {
            currentHeight = height;
            currentMass = mass;

            bmiResult = bmiCalculator.countBMI(Float.parseFloat(mass), Float.parseFloat(height));
            successfullyCalculated = true;
        }
        catch (IllegalArgumentException e)
        {
            optionalErrorMessage = e.getMessage();
            successfullyCalculated = false;
            saveMassAndHeightOptionEnabled = false;
        }

        if (successfullyCalculated)
        {
            view.showBmi(String.format(Locale.ENGLISH, "%.2f", bmiResult));
            adjustViewBasedOnBmi(bmiResult);
            saveMassAndHeightOptionEnabled = true;
        }
        else
        {
            view.showMessage(optionalErrorMessage);
        }
    }

    private void adjustViewBasedOnBmi(float bmiResult)
    {
        @StringRes
        int bmiInfoId;
        @ColorRes
        int bmiColorId;

        if (bmiResult > BmiConstants.overweight)
        {
            bmiInfoId = R.string.bmi_overweight;
            bmiColorId = R.color.highBmi;
        }
        else if (bmiResult < BmiConstants.underweight)
        {
            bmiInfoId = R.string.bmi_underweight;
            bmiColorId = R.color.lowBmi;
        }
        else
        {
            bmiInfoId = R.string.bmi_ok;
            bmiColorId = R.color.normalBmi;
        }

        view.showInfoAboutBmi(bmiInfoId);
        view.changeColorOfBmiResult(bmiColorId);
    }
}
