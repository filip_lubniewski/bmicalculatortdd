package filiplubniewski.lab1.bmi;

/**
 * Created by macbookpro on 29.03.2017.
 */

public final class BmiConstants
{
    public static final float underweight = 18.5f;
    public static final float overweight = 25.0f;

    public static final int KG_AND_M = 0;
    public static final int LBS_AND_FT = 1;

    private BmiConstants() {}
}
