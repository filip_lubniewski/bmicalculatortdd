package filiplubniewski.lab1.bmi;

import android.support.annotation.StringRes;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import filiplubniewski.lab1.bmi.bmiCalculator.BmiCalculatorUsingKilogramsAndMeters;
import filiplubniewski.lab1.bmi.bmiContract.BmiActivityView;
import filiplubniewski.lab1.bmi.bmiContract.BmiCalculator;
import filiplubniewski.lab1.bmi.presenter.BmiActivityPresenter;

/**
 * Created by macbookpro on 28.03.2017.
 */
public class BmiActivityPresenterTest
{
    private BmiActivityPresenter presenter;
    private BmiCalculator bmiCalculator;
    private BmiActivityView view;

    @Before
    public void setUp()
    {
        view = new MockView();
        bmiCalculator = new BmiCalculatorUsingKilogramsAndMeters();
        presenter = new BmiActivityPresenter(view, bmiCalculator);
    }

    @Test
    public void shouldCalculateBmiAndPassResultToTheView()
    {
        //given
        String givenHeight = "1.8";
        String givenMass = "70";

        //when
        presenter.passUserInputToBmiCalculator(givenMass, givenHeight);

        //then
        Assert.assertEquals(true, ((MockView) view).bmiDisplayed);
    }

    @Test
    public void shouldDisplayMessageIfInputValueIsInvalid()
    {
        //given
        String givenHeight = "-1.8";
        String givenMass = "70";

        //when
        presenter.passUserInputToBmiCalculator(givenMass, givenHeight);

        //then
        Assert.assertEquals(true, ((MockView) view).wrongInputRangeMessageDisplayed);
    }

    @Test
    public void shouldDisplayMessageIfInputIsEmpty()
    {
        //given
        String givenHeight = "";
        String givenMass = "70";

        //when
        presenter.passUserInputToBmiCalculator(givenMass, givenHeight);

        //then
        Assert.assertEquals(true, ((MockView) view).emptyInputMessageDisplayed);
    }

    @Test
    public void shouldDisplayInfoAboutBmi()
    {
        //given
        String givenHeight = "1.8";
        String givenMass = "65";

        //when
        presenter.passUserInputToBmiCalculator(givenMass, givenHeight);

        //then
        Assert.assertEquals(true, ((MockView) view).infoAboutBmiDisplayed);
    }

    @Test
    public void shouldChangeColorOfBmiResultForRed()
    {
        //given
        String givenHeight = "1.5";
        String givenMass = "100";

        //when
        presenter.passUserInputToBmiCalculator(givenMass, givenHeight);

        //then
        Assert.assertEquals(R.color.highBmi, ((MockView) view).colorId);
    }

    private class MockView implements BmiActivityView
    {
        boolean bmiDisplayed;
        boolean wrongInputRangeMessageDisplayed;
        boolean emptyInputMessageDisplayed;
        boolean infoAboutBmiDisplayed;

        int colorId;

        @Override
        public void showBmi(String bmiValue)
        {
            bmiDisplayed = true;
        }

        @Override
        public void showMessage(@StringRes int messageId)
        {
            if (messageId == R.string.message_empty_input)
            {
                emptyInputMessageDisplayed = true;
            }
        }

        @Override
        public void showMessage(String msg)
        {
            if (msg.equals("Wrong mass or height. Should be (10 - 250kg) and (0.5 - 10m)"))
            {
                wrongInputRangeMessageDisplayed = true;
            }
        }

        @Override
        public void showInfoAboutBmi(@StringRes int infoId)
        {
            infoAboutBmiDisplayed = true;
        }

        @Override
        public void changeColorOfBmiResult(@StringRes int colorId)
        {
            this.colorId = colorId;
        }

        @Override
        public void saveUserMassAndHeight(String mass, String height, int units)
        {

        }
    }
}