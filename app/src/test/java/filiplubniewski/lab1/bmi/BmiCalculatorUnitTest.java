package filiplubniewski.lab1.bmi;

import org.junit.Test;

import filiplubniewski.lab1.bmi.bmiCalculator.BmiCalculatorUsingKilogramsAndMeters;
import filiplubniewski.lab1.bmi.bmiContract.BmiCalculator;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


/**
 * Created by macbookpro on 16.03.2017.
 */

public class BmiCalculatorUnitTest
{
    @Test
    public void mass_below_0_isInvalid() throws Exception
    {
        //given
        float testMass = -1f;
        //when
        BmiCalculator bmiCalculatorUsingKilograms = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        boolean actualMass = bmiCalculatorUsingKilograms.isMassValid(testMass);

        assertFalse(actualMass);
    }

    @Test
    public void mass_above_250_isInvalid() throws Exception
    {
        //given
        float testMass = 250.1f;

        //when
        BmiCalculator bmiCalculatorUsingKilograms = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        boolean actualMass = bmiCalculatorUsingKilograms.isMassValid(testMass);

        assertFalse(actualMass);
    }

    @Test
    public void mass_equals_0_isInvalid() throws Exception
    {
        //given
        float testMass = 0.0f;

        //when
        BmiCalculator bmiCalculatorUsingKilograms = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        boolean actualMass = bmiCalculatorUsingKilograms.isMassValid(testMass);

        assertFalse(actualMass);
    }

    @Test
    public void mass_positive_and_below_10_isInvalid() throws Exception
    {
        //given
        float testMass = 9.99f;

        //when
        BmiCalculator bmiCalculatorUsingKilograms = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        boolean actualMass = bmiCalculatorUsingKilograms.isMassValid(testMass);

        assertFalse(actualMass);
    }

    @Test
    public void mass_above_10_and_below_250_isValid() throws Exception
    {
        //given
        float testMass = 70f;

        //when
        BmiCalculator bmiCalculatorUsingKilograms = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        boolean actualMass = bmiCalculatorUsingKilograms.isMassValid(testMass);

        assertTrue(actualMass);
    }

    @Test
    public void height_below_0_isInvalid() throws Exception
    {
        //given
        float testHeight = -1f;
        //when
        BmiCalculator bmiCalculatorUsingMeters = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        boolean actualHeight = bmiCalculatorUsingMeters.isHeightValid(testHeight);

        assertFalse(actualHeight);
    }

    @Test
    public void height_above_10_isInvalid() throws Exception
    {
        //given
        float testHeight = 13.1f;
        //when
        BmiCalculator bmiCalculatorUsingMeters = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        boolean actualHeight = bmiCalculatorUsingMeters.isHeightValid(testHeight);

        assertFalse(actualHeight);
    }

    @Test
    public void height_equals_0_isInvalid() throws Exception
    {
        //given
        float testHeight = 0f;
        //when
        BmiCalculator bmiCalculatorUsingMeters = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        boolean actualHeight = bmiCalculatorUsingMeters.isHeightValid(testHeight);

        assertFalse(actualHeight);
    }

    @Test
    public void height_positive_and_below_half_meter_isInvalid() throws Exception
    {
        //given
        float testHeight = 0.499f;
        //when
        BmiCalculator bmiCalculatorUsingMeters = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        boolean actualHeight = bmiCalculatorUsingMeters.isHeightValid(testHeight);

        assertFalse(actualHeight);
    }

    @Test
    public void height_above_half_meter_and_below_10_meters_isValid() throws Exception
    {
        //given
        float testHeight = 2f;
        //when
        BmiCalculator bmiCalculatorUsingMeters = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        boolean actualHeight = bmiCalculatorUsingMeters.isHeightValid(testHeight);

        assertTrue(actualHeight);
    }

    @Test
    public void valid_mass_and_valid_height_results_in_valid_BMI()
    {
        //given
        float testMass = 65f;
        float testHeight = 1.8f;
        float acceptableDelta = 0.1f;

        //when
        BmiCalculator bmiCalculatorUsingMetersAndKilograms = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        float actualBMI = bmiCalculatorUsingMetersAndKilograms.countBMI(testMass, testHeight);
        assertEquals(20f, actualBMI, acceptableDelta);
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalid_mass_is_not_allowed_while_calculating_bmi()
    {
        //given
        float testMass = 65f;
        float testHeight = -1.8f;

        //when
        BmiCalculator bmiCalculatorUsingMetersAndKilograms = new BmiCalculatorUsingKilogramsAndMeters();

        //then
        bmiCalculatorUsingMetersAndKilograms.countBMI(testMass, testHeight);
    }

    //TODO:
    //edge cases
    // 0 values
    // expected values validating
    //calculations functions, assertEquals with delta error +-someValue
    // illegalArgumentException case
}
