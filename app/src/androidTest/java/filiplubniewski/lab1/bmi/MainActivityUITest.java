package filiplubniewski.lab1.bmi;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by macbookpro on 29.03.2017.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityUITest
{
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void shouldUpdateResultTextViewAfterProvidingMassAndHeightAndClickingCountButton()
    {
        //given
        String givenMass = "60";
        String givenHeight = "1.8";
        onView(withId(R.id.edit_text_mass)).perform(typeText(givenMass), pressImeActionButton());
        onView(withId(R.id.edit_text_height)).perform(typeText(givenHeight), pressImeActionButton());

        //when
        onView(withId(R.id.button_count)).perform(click());

        //then
        onView(withId(R.id.text_view_result)).check(matches(withText("18.52")));
    }
}
